var app = angular.module("submitVAT", []);

app.controller('VatController', function($scope, $http) {
      $scope.submit = function() {
        $scope.InfoText = "Waiting for query result";
        $scope.Notification = true;
        $scope.NotificationType = "info";

        $http({
            method: 'GET',
            url: 'https://vat.erply.com/numbers?vatNumber=' + $scope.text
        }).then(function (response){
            $scope.Address      = response.data["Address"];
            $scope.Name         = response.data["Name"];
            $scope.RequestDate  = response.data["RequestDate"];
            $scope.VAT          = response.data["VATNumber"];
            $scope.Valid        = response.data["Valid"];
            $scope.Notification = false;
            $scope.isVisible = true;
        },function (error){
            $scope.NotificationType = "error";
            $scope.InfoText = error.data["error"];
        });
      }
 })
